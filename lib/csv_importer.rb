# encoding: utf-8

require 'net/sftp'
require 'iconv'
require 'csv'

class CsvImporter
  NIL_BLZ = '00000000'.freeze
  LASTSCHRIFT_BLZ = '70022200'.freeze

  UMSATZ_KEY_A = '10'.freeze
  UMSATZ_KEY_B = '16'.freeze

  @sftp_server = if Rails.env == 'production'
                   'csv.example.com/endpoint/'
                 else
                   '0.0.0.0:2020'
                 end
  @errors = []

  cattr_accessor :import_retry_count

  class << self
    attr_reader :errors, :sftp_server

    def transfer(send_email = true)
      FileUtils.mkdir_p "#{Rails.root}/private/data/download"
      Net::SFTP.start(@sftp_server, 'some-ftp-user',
                      keys: ['path-to-credentials']) do |sftp|
        sftp_entries = sftp.dir.entries('/data/files/csv').map(&:name).sort
        sftp_entries.each do |entry|
          next unless File.extname(entry) == '.csv'
          next unless sftp_entries.include?(entry + '.start')

          file_local = "#{Rails.root}/private/data/download/#{entry}"
          file_remote = "/data/files/csv/#{entry}"

          sftp.download!(file_remote, file_local)
          sftp.remove!(file_remote + '.start')

          result = import(file_local)

          if result == 'Success'
            File.delete(file_local)

            if send_email
              BackendMailer.send_import_feedback(
                'Successful Import',
                "Import of the file #{entry} done."
              )
            end
          else
            error_content = [
              "Import of the file #{entry} failed with errors:",
              result
            ].join("\n")
            upload_error_file(entry, error_content)

            if send_email
              BackendMailer.send_import_feedback(
                'Import CSV failed',
                error_content
              )
            end

            break
          end
        end
      end
    end

    def import(file, validation_only = false)
      begin
        result = import_file(file, validation_only)
      rescue => e
        result = { errors: [e], success: ['data lost'] }
      end

      result = if result[:errors].blank?
                 'Success'
               else
                 "Imported rows: #{result[:success].join(', ')};" \
                 " Errors: #{result[:errors].join('; ')}"
               end

      Rails.logger.info(
        "CsvImporter#import time: #{Time.now.to_formatted_s(:db)};" \
        " Imported file '#{file}': #{result}"
      )

      result
    end

    def import_file(file, validation_only = false)
      @errors = []
      line = 2
      source_path = "#{Rails.root}/private/upload"
      path_and_name = "#{source_path}/csv/tmp_mraba/DTAUS" \
                      "#{Time.now.strftime('%Y%m%d_%H%M%S')}"
      FileUtils.mkdir_p "#{source_path}/csv/tmp_mraba"

      @dtaus = Mraba::Transaction.define_dtaus(
        'RS',
        8_888_888_888,
        99_999_999,
        'Credit collection'
      )
      success_rows = []
      import_rows = CSV.read(
        file,
        { col_sep: ';', headers: true, skip_blanks: true }
      ).map { |r| [r.to_hash['ACTIVITY_ID'], r.to_hash] }

      import_rows.each do |index, row|
        next if index.blank?
        break unless validate_import_row(row)
        import_file_row_with_error_handling(row, validation_only,
                                            @errors, @dtaus)
        line += 1
        break unless @errors.empty?
        success_rows << row['ACTIVITY_ID']
      end

      if @errors.empty? && !validation_only
        unless @dtaus.is_empty?
          @dtaus.add_datei("#{path_and_name}_201_mraba.csv")
        end
      end

      { success: success_rows, errors: @errors }
    end

    def import_file_row(row, validation_only, errors, dtaus)
      case transaction_type(row)
      when 'AccountTransfer' then add_account_transfer(row, validation_only)
      when 'BankTransfer' then add_bank_transfer(row, validation_only)
      when 'Lastschrift' then add_dta_row(dtaus, row, validation_only)
      else errors << "#{row['ACTIVITY_ID']}: Transaction type not found"
      end

      [errors, dtaus]
    end

    def import_file_row_with_error_handling(row, validation_only, errors, dtaus)
      max_attempts = 5
      error_text = nil
      self.import_retry_count = 0

      max_attempts.times do |i|
        self.import_retry_count += 1
        error_text = nil
        begin
          import_file_row(row, validation_only, errors, dtaus)
          break
        rescue => e
          error_text = "#{row['ACTIVITY_ID']}: #{e}" if (i + 1) == max_attempts
        end
      end
      errors << error_text if error_text

      [errors, dtaus]
    end

    def validate_import_row(row)
      size = @errors.size

      unless [UMSATZ_KEY_A, UMSATZ_KEY_B].include? row['UMSATZ_KEY']
        @errors << "#{row['ACTIVITY_ID']}: UMSATZ_KEY #{row['UMSATZ_KEY']}" \
                   " is not allowed"
      end

      size == @errors.size
    end

    def transaction_type(row)
      return 'AccountTransfer' if row['SENDER_BLZ'] == NIL_BLZ &&
                                  row['RECEIVER_BLZ'] == NIL_BLZ
      return 'BankTransfer' if row['SENDER_BLZ'] == NIL_BLZ &&
                               row['UMSATZ_KEY'] == UMSATZ_KEY_A
      return 'Lastschrift' if row['RECEIVER_BLZ'] == LASTSCHRIFT_BLZ &&
                              row['UMSATZ_KEY'] == UMSATZ_KEY_B
      false
    end

    def get_sender(row)
      sender = Account.find_by_account_no(row['SENDER_KONTO'])

      if sender.nil?
        @errors << "#{row['ACTIVITY_ID']}: Account #{row['SENDER_KONTO']}" \
                   " not found"
      end

      sender
    end

    def add_account_transfer(row, validation_only)
      sender = get_sender(row)
      return @errors.last unless sender

      if row['DEPOT_ACTIVITY_ID'].blank?
        account_transfer = sender.credit_account_transfers.build(
          amount: row['AMOUNT'].to_f,
          subject: import_subject(row),
          receiver_multi: row['RECEIVER_KONTO']
        )
        account_transfer.date = row['ENTRY_DATE'].to_date
        account_transfer.skip_mobile_tan = true
      else
        account_transfer = sender.credit_account_transfers
                                 .find_by_id(row['DEPOT_ACTIVITY_ID'])
        if account_transfer.nil?
          @errors << "#{row['ACTIVITY_ID']}: AccountTransfer not found"
          return
        elsif account_transfer.state != 'pending'
          @errors << "#{row['ACTIVITY_ID']}: AccountTransfer state expected" \
                     " 'pending' but was '#{account_transfer.state}'"
          return
        else
          account_transfer.subject = import_subject(row)
        end
      end
      if account_transfer && !account_transfer.valid?
        full_err_msgs = account_transfer.errors.full_messages.join('; ')
        @errors << "#{row['ACTIVITY_ID']}: AccountTransfer validation " \
                   "error(s): #{full_err_msgs}"
      elsif !validation_only
        if row['DEPOT_ACTIVITY_ID'].blank?
          account_transfer.save!
        else
          account_transfer.complete_transfer!
        end
      end
    end

    def add_bank_transfer(row, validation_only)
      sender = get_sender(row)
      return @errors.last unless sender

      bank_transfer = sender.build_transfer(
        amount: row['AMOUNT'].to_f,
        subject: import_subject(row),
        rec_holder: row['RECEIVER_NAME'],
        rec_account_number: row['RECEIVER_KONTO'],
        rec_bank_code: row['RECEIVER_BLZ']
      )

      if !bank_transfer.valid?
        full_err_msgs = bank_transfer.errors.full_messages.join('; ')
        @errors << "#{row['ACTIVITY_ID']}: BankTransfer validation error(s):" \
                   " #{full_err_msgs}"
      elsif !validation_only
        bank_transfer.save!
      end
    end

    def add_dta_row(dtaus, row, _validation_only)
      unless dtaus.valid_sender?(row['SENDER_KONTO'], row['SENDER_BLZ'])
        return @errors << "#{row['ACTIVITY_ID']}: BLZ/Konto not valid, " \
                          "csv file not written"
      end
      holder = Iconv.iconv('ascii//translit', 'utf-8', row['SENDER_NAME'])
                    .to_s.gsub(/[^\w^\s]/, '')
      dtaus.add_buchung(
        row['SENDER_KONTO'],
        row['SENDER_BLZ'],
        holder,
        BigDecimal(row['AMOUNT']).abs,
        import_subject(row)
      )
    end

    def import_subject(row)
      subject = ''

      (1..14).each do |id|
        subject += row["DESC#{id}"].to_s unless row["DESC#{id}"].blank?
      end

      subject
    end

    private

    def upload_error_file(entry, result)
      FileUtils.mkdir_p "#{Rails.root}/private/data/upload"
      error_file = "#{Rails.root}/private/data/upload/#{entry}"
      File.write(error_file, result)
      Net::SFTP.start(@sftp_server, 'some-ftp-user',
                      keys: ['path-to-credentials']) do |sftp|
        sftp.upload!(error_file, "/data/files/batch_processed/#{entry}")
      end
    end
  end
end
