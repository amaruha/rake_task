# encoding: utf-8
require 'spec_helper'
require_relative '../lib/csv_importer'

# Fakes for outside objects
class Account
  def self.find_by_account_no(*)
  end
end

module Mraba
  class Transaction
    def self.define_dtaus(*args)
      new
    end

    def valid_sender?(*)
    end

    def add_buchung(*)
    end

    def add_datei(*)
    end

    def is_empty?(*)
    end
  end
end

module BackendMailer
  extend self

  def send_import_feedback(*args)
  end
end

describe CsvImporter do
  describe '.transfer(send_email = true)' do
    let(:download_folder) { "#{Rails.root}/private/data/download" }
    let(:download_file) { "#{download_folder}/mraba.csv" }
    let(:remote_file) { '/data/files/csv/mraba.csv' }

    before(:each) do
      entries = ['mraba.csv', 'mraba.csv.start', 'blubb.csv']
      sftp_mock = double('sftp')
      Net::SFTP.stub(:start).and_yield(sftp_mock)
      sftp_mock.stub_chain(:dir, :entries, :map).and_return(entries)
      sftp_mock.stub(:download!).with(remote_file, download_file)
      sftp_mock.stub(:remove!).with("#{remote_file}.start")
    end

    it 'fails transfers mraba csv' do
      BackendMailer.should_receive(:send_import_feedback)
                   .with('Import CSV failed',
                         "Import of the file mraba.csv " \
                         "failed with errors:\nSome error")
                   .once
      CsvImporter.should_receive(:upload_error_file)
                 .with('mraba.csv',
                       "Import of the file mraba.csv " \
                       "failed with errors:\nSome error")
                 .once
      CsvImporter.should_receive(:import).with(download_file).once
                 .and_return(['Some error'])
      FileUtils.should_receive(:mkdir_p).with(download_folder).once
      CsvImporter.transfer
    end

    it 'fails transfers mraba csv without notify' do
      BackendMailer.should_not_receive(:send_import_feedback)
      CsvImporter.should_receive(:upload_error_file)
                 .with('mraba.csv',
                       "Import of the file mraba.csv " \
                       "failed with errors:\nSome error")
                 .once
      CsvImporter.should_receive(:import).with(download_file).once
                 .and_return(['Some error'])
      FileUtils.should_receive(:mkdir_p).with(download_folder).once
      CsvImporter.transfer(false)
    end

    it 'transfers mraba csv' do
      BackendMailer.should_receive(:send_import_feedback)
                   .with('Successful Import',
                         'Import of the file mraba.csv done.')
      File.should_receive(:delete).with(download_file).once
      CsvImporter.should_receive(:import).with(download_file).once
                 .and_return('Success')
      FileUtils.should_receive(:mkdir_p).with(download_folder).once
      CsvImporter.transfer
    end

    it 'transfers mraba csv without notify' do
      BackendMailer.should_not_receive(:send_import_feedback)
      File.should_receive(:delete).with(download_file).once
      CsvImporter.should_receive(:import).with(download_file).once
                 .and_return('Success')
      FileUtils.should_receive(:mkdir_p).with(download_folder).once
      CsvImporter.transfer(false)
    end
  end

  describe '.import(file, validation_only = false)' do
    let(:downloaded_file) { 'mtab.csv' }
    let(:formatted_time) { '1970-1-1' }

    before(:each) do
      time_mock = double('time')
      Time.stub(:now).and_return(time_mock)
      time_mock.stub(:to_formatted_s).with(:db).and_return(formatted_time)
    end

    it 'successful import' do
      Rails.logger.should_receive(:info)
           .with("CsvImporter#import time: #{formatted_time}; " \
                 "Imported file '#{downloaded_file}': Success")
           .once
      CsvImporter.should_receive(:import_file).with(downloaded_file, false)
                 .and_return({ success: %w[one two], errors: [] })
      CsvImporter.import(downloaded_file).should == 'Success'
    end

    it 'handles exception during import' do
      Rails.logger.should_receive(:info)
           .with("CsvImporter#import time: #{formatted_time}; " \
                 "Imported file '': Imported rows: data lost; " \
                 "Errors: RuntimeError")
           .once
      CsvImporter.stub(:import_file).and_raise(RuntimeError)
      CsvImporter.should_receive(:import_file).with(nil, false)
      CsvImporter.import(nil).should == \
        'Imported rows: data lost; Errors: RuntimeError'
    end

    it 'handles errors during import' do
      Rails.logger.should_receive(:info)
           .with("CsvImporter#import time: #{formatted_time};" \
                 " Imported file '#{downloaded_file}': " \
                 "Imported rows: one; Errors: Ups!")
           .once
      CsvImporter.should_receive(:import_file).with(downloaded_file, false)
                 .and_return({ success: %w[one], errors: %w[Ups!] })
      CsvImporter.import(downloaded_file).should == \
        'Imported rows: one; Errors: Ups!'
    end
  end

  describe '.import_file(file, validation_only = false)' do
    before(:each) do
      @data = {
        'ACTIVITY_ID' => '123',
        'AMOUNT' => '5',
        'UMSATZ_KEY' => '10',
        'ENTRY_DATE' => Time.now.strftime('%Y%m%d'),
        'KONTONUMMER' => '000000001',
        'RECEIVER_BLZ' => '00000000',
        'RECEIVER_KONTO' => '000000002',
        'RECEIVER_NAME' => 'Mustermann',
        'SENDER_BLZ' => '00000000',
        'SENDER_KONTO' => '000000003',
        'SENDER_NAME' => 'Mustermann',
        'DESC1' => 'Geld senden'
      }

      CSV.stub_chain(:read, :map).and_return [['123', @data]]
    end

    it 'passed import row validation' do
      FileUtils.should_receive(:mkdir_p).once
      CsvImporter.should_receive(:validate_import_row).with(@data)
                 .and_return(true)
      CsvImporter.should_receive(:import_file_row_with_error_handling)
      CsvImporter.import_file('file').should == { success: ['123'], errors: [] }
    end
  end

  describe '.import_file_row(row, validation_only, errors, dtaus)' do
    let(:dtaus) { 'stub' }
    let(:validation) { 'false' }
    let(:row) { { 'ACTIVITY_ID' => '123' } }
    let(:errors) { [] }

    it 'account transfer import row' do
      CsvImporter.stub(:transaction_type).and_return('AccountTransfer')
      CsvImporter.should_receive(:add_account_transfer).with(row, validation)
      CsvImporter.import_file_row(row, validation, errors, dtaus).should == \
        [errors, dtaus]
    end

    it 'bank transfer import row' do
      CsvImporter.stub(:transaction_type).and_return('BankTransfer')
      CsvImporter.should_receive(:add_bank_transfer).with(row, validation)
      CsvImporter.import_file_row(row, validation, errors, dtaus).should == \
        [errors, dtaus]
    end

    it 'lastschirft import row' do
      CsvImporter.stub(:transaction_type).and_return('Lastschrift')
      CsvImporter.should_receive(:add_dta_row).with(dtaus, row, validation)
      CsvImporter.import_file_row(row, validation, errors, dtaus).should == \
        [errors, dtaus]
    end

    it 'handles error during import row' do
      CsvImporter.stub(:transaction_type).and_return('Unknown type')
      CsvImporter.import_file_row(row, validation, errors, dtaus).should == \
        [['123: Transaction type not found'], dtaus]
    end
  end

  describe '.import_file_row_with_error_handling(' \
           'row, validation_only = false)' do
    before(:each) do
      @errors = []
      @dtaus = double
      CsvImporter.import_retry_count = 0
    end

    it 'successful import' do
      row = {
        'RECEIVER_BLZ' => '00000000',
        'SENDER_BLZ' => '00000000',
        'ACTIVITY_ID' => '1'
      }

      CsvImporter.stub(:add_account_transfer).and_return(true)
      CsvImporter.import_file_row_with_error_handling(row, false,
                                                      @errors, @dtaus)
                 .should eq [@errors, @dtaus]
      CsvImporter.import_retry_count.should == 1
    end

    it 'handles exception in row' do
      row = {
        'RECEIVER_BLZ' => '00000000',
        'SENDER_BLZ' => '00000000',
        'ACTIVITY_ID' => '1'
      }

      CsvImporter.stub(:add_account_transfer).and_raise(RuntimeError)
      CsvImporter.send(:import_file_row_with_error_handling, row, false,
                       @errors, @dtaus)[0].should eq ['1: RuntimeError']
      CsvImporter.import_retry_count.should == 5
    end
  end

  describe '.validate_import_row(row)' do
    after(:all) do
      CsvImporter.instance_variable_set(:@errors, [])
    end

    before(:each) do
      CsvImporter.instance_variable_set(:@errors, [])
    end

    it 'handles failed validation' do
      row = { 'UMSATZ_KEY' => 1_230_123 }
      CsvImporter.instance_variable_get(:@errors).should eq []
      CsvImporter.validate_import_row(row).should be false
      CsvImporter.instance_variable_get(:@errors).should == \
        [': UMSATZ_KEY 1230123 is not allowed']
    end

    it 'validation passed' do
      row = { 'UMSATZ_KEY' => CsvImporter::UMSATZ_KEY_A }
      CsvImporter.validate_import_row(row).should be true
    end
  end

  describe '.transaction_type(row)' do
    it "returns 'AccountTransfer'" do
      row = { 'SENDER_BLZ' => '00000000', 'RECEIVER_BLZ' => '00000000' }
      CsvImporter.transaction_type(row).should == 'AccountTransfer'
    end

    it "returns 'BankTransfer'" do
      row = { 'SENDER_BLZ' => '00000000', 'UMSATZ_KEY' => '10' }
      CsvImporter.transaction_type(row).should == 'BankTransfer'
    end

    it "returns 'Lastschrift'" do
      row = { 'RECEIVER_BLZ' => '70022200', 'UMSATZ_KEY' => '16' }
      CsvImporter.transaction_type(row).should == 'Lastschrift'
    end

    it "returns 'false'" do
      row = {}
      CsvImporter.transaction_type(row).should be false
    end
  end

  describe '.get_sender(row)' do
    after(:all) do
      CsvImporter.instance_variable_set(:@errors, [])
    end

    before(:each) do
      CsvImporter.instance_variable_set(:@errors, [])
      @account = double
      Account.stub(:find_by_account_no).with('000000001') { @account }
      @row = { 'SENDER_KONTO' => '000000001' }
    end

    it "finds sender via 'SENDER_KONTO' column" do
      CsvImporter.get_sender(@row).should == @account
    end

    it "fails to find sender via 'SENDER_KONTO' column" do
      @account = nil
      CsvImporter.get_sender(@row).should be nil
    end
  end

  describe '.add_account_transfer(row, validation_only)' do
    before(:each) do
      @account = double :account_no => '000000001'
      @row = {
        'AMOUNT' => 10,
        'ENTRY_DATE' => Date.today,
        'DESC1' => 'Subject',
        'SENDER_KONTO' => '000000001',
        'RECEIVER_KONTO' => '000000002'
      }
      Account.stub(:find_by_account_no).with('000000001').and_return @account
      @account_transfer = double(
        :date= => nil, :skip_mobile_tan= => nil,
        :valid? => nil, :errors => double(:full_messages => []),
        :save! => true
      )
      @account.stub_chain :credit_account_transfers, :build => @account_transfer
    end

    it 'adds account_transfer (DEPOT_ACTIVITY_ID is blank)' do
      @account_transfer.stub :valid? => true
      CsvImporter.add_account_transfer(@row, false).should be true
    end

    it 'fails to add a account_transfer (missing attribute)' do
      @row['AMOUNT'] = nil
      CsvImporter.add_account_transfer(@row, false).should be_kind_of(Array)
    end

    it 'fails to add a account_transfer (missing attribute, validation only)' do
      @row['AMOUNT'] = nil
      CsvImporter.add_account_transfer(@row, true).should be_kind_of(Array)
    end

    it 'returns error' do
      Account.stub(:find_by_account_no).with('000000001').and_return nil
      CsvImporter.add_account_transfer({ 'SENDER_KONTO' => '000000001' }, false)
                 .should eq ': Account 000000001 not found'
    end

    context 'DEPOT_ACTIVITY_ID is not blank' do
      before(:each) do
        @account_transfer.stub(
          :id => 1, :state => 'pending',
          :subject= => nil, :valid? => true,
          :complete_transfer! => true
        )
        @account.stub_chain(:credit_account_transfers,
                            find_by_id: @account_transfer)
      end

      it 'finds and validates account_transfer' do
        @row['DEPOT_ACTIVITY_ID'] = @account_transfer.id

        @account_transfer.should_receive :complete_transfer!

        CsvImporter.add_account_transfer(@row, false).should eq true
      end

      it 'fails to find account transfer' do
        @row['DEPOT_ACTIVITY_ID'] = '12345'
        @account.stub_chain :credit_account_transfers, :find_by_id => nil
        CsvImporter.add_account_transfer(@row, false).should be nil
      end

      it 'finds account transfer, but is not in pending state' do
        @account_transfer.stub :state => 'initialized'
        @row['DEPOT_ACTIVITY_ID'] = @account_transfer.id
        CsvImporter.add_account_transfer(@row, false).should be nil
      end
    end
  end

  describe '.add_bank_transfer(row, validation_only)' do
    before(:each) do
      @row = {
        'AMOUNT' => 10,
        'RECEIVER_NAME' => 'Bob Baumeiter',
        'RECEIVER_BLZ' => '2222222',
        'DESC1' => 'Subject',
        'SENDER_KONTO' => '000000001',
        'RECEIVER_KONTO' => '000000002'
      }
      bank_transfer = double :valid? => true, :save! => true
      @account = double :build_transfer => bank_transfer
    end

    it 'adds bank transfer' do
      Account.stub(:find_by_account_no).with('000000001').and_return @account
      CsvImporter.add_bank_transfer(@row, false).should be true
    end

    it 'fails to add bank transfer' do
      Account.stub(:find_by_account_no).with('000000001').and_return nil
      CsvImporter.add_bank_transfer(@row, false).should eq \
        ': Account 000000001 not found'
    end
  end

  describe '.add_dta_row(dta, row, validation_only)' do
    before(:each) do
      @row = {
        'ACTIVITY_ID' => '1',
        'AMOUNT' => '10',
        'RECEIVER_NAME' => 'Bob Baumeiter',
        'RECEIVER_BLZ' => '70022200',
        'DESC1' => 'Subject',
        'SENDER_KONTO' => '0101881952',
        'SENDER_BLZ' => '30020900',
        'SENDER_NAME' => 'Max Müstermänn',
        'RECEIVER_KONTO' => 'NO2'
      }
      @dtaus = double
    end

    it 'adds dta row' do
      @dtaus.stub(:valid_sender?).with('0101881952', '30020900')
            .and_return true
      @dtaus.should_receive(:add_buchung).with('0101881952', '30020900',
                                               'Max Mustermann', 10, 'Subject')
      CsvImporter.add_dta_row(@dtaus, @row, false)
    end

    it 'fails to adds dta row' do
      @dtaus.stub(:valid_sender?).with('0101881952', '30020900')
            .and_return false
      @dtaus.should_not_receive(:add_buchung)
      CsvImporter.add_dta_row(@dtaus, @row, false).last.should ==
        '1: BLZ/Konto not valid, csv file not written'
    end
  end

  describe '.import_subject(row)' do
    before(:each) do
      @row = { 'DESC1' => 'Sub', 'DESC2' => 'ject' }
    end

    it 'returns subject from row' do
      CsvImporter.import_subject(@row).should == 'Subject'
    end
  end

  describe '.upload_error_file(entry, result)' do
    let(:upload_dir) { "#{Rails.root}/private/data/upload" }
    let(:batch_dir) { '/data/files/batch_processed' }
    let(:entry) { 'mraba.csv' }
    let(:result) { 'some result' }

    before(:each) do
      @sftp_mock = double('sftp')
      Net::SFTP.stub(:start).and_yield(@sftp_mock)
    end

    it 'uploads error file' do
      @sftp_mock.should_receive(:upload!)
                .with("#{upload_dir}/#{entry}", "#{batch_dir}/#{entry}")
                .once
      File.should_receive(:write).with("#{upload_dir}/#{entry}", result).once
      FileUtils.should_receive(:mkdir_p).with(upload_dir).once
      CsvImporter.send(:upload_error_file, entry, result).should be nil
    end
  end
end
